class phpro-dev01-3
{

#### This will set-up the box
#### Install packages in default.pp only

notice('PHProbox.pp is loading')

###################
# Automatic Proxy #
###################
 
exec
{
  "autoproxy":

  command => "/vagrant/vagrant/scripts/setproxy",
  user    => "root"
}
 
#######################
# Replace Bashrc File #
#######################
 
file
{ 
  "bashrc":

  path    => "/home/vagrant/.bashrc",
  ensure  => present,
  replace => true,
  owner   => vagrant,
  group   => vagrant,
  source  => "/vagrant/vagrant/modules/bashrc/.bashrc",
}

########################
# Replace Profile File #
########################

file
{
  "profile":

  path    => "/root/.profile",
  ensure  => present,
  replace => true,
  owner   => root,
  group   => root,
  source  => "/vagrant/vagrant/modules/profile/.profile",
}
 
#######################
# Set NFS Permissions #
#######################
 
if $nfsorsf == '1'
{
  notice('NFS share is mounted, setting correct permissions')
  exec
  {
    "setnfsid":

    command => "/vagrant/vagrant/scripts/setnfsid",
    user    => "root",
  }
}
else
{
  notice('No NFS server available, using built-in solution')
}

###########################################################################################################
# Load Default if not in comment. This one should ONLY be in comment when no internet access is available #
###########################################################################################################

if $internet == 'Yes'
{
  include framework::default
} 
}
include phpro-dev01-3
