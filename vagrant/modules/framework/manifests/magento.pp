class framework::magento 
{

notice('magento.pp is loading')

include apache::apache
include php::php
include mysql::mysql
include phpmyadmin::phpmyadmin

############
# Wiz Tool #
############

exec
{
  "install-wiz": 

  command => "/vagrant/vagrant/scripts/installwiz", 
  user => "root"
}

###############
# N98-Magerun #
###############

exec
{ 
  "installmagerun":

  command => "/usr/bin/wget -O /usr/local/bin/n98-magerun.phar https://raw.github.com/netz98/n98-magerun/master/n98-magerun.phar",
  user    => "root"
}

file 
{ 
  "/usr/local/bin/n98-magerun.phar":

  mode => 755,
  require => Exec["installmagerun"]
}

##########
# Modman #
##########

exec 
{ 
  "installmodman":

  command => "/usr/bin/wget -O /usr/local/bin/modman https://raw.github.com/colinmollenhour/modman/master/modman",
  user    => "root"
}

file { "/usr/local/bin/modman":
        mode => 755,
        require => Exec["installmodman"]}

#################
#link src folder#
#################

file 
{ 
  "/var/www/src":

  ensure => 'link',
  target => '/vagrant/src',
  owner => www-data,
  group => vagrant,
  mode => 755,
  require => Package["apache2"]
}

########
# Solr #
########

package
{
  "openjdk-6-jdk":

  ensure => "installed",
  require => Exec["apt-get update"]
}

exec
{
  "install-solr":

  command => "/vagrant/vagrant/scripts/installsolr-magento",
  require => Package["openjdk-6-jdk"],
  user => "root"
}

file 
{ 
  "/opt/apache-solr-3.5.0/phpro/":

  require => Exec["install-solr"],
  ensure => directory,
  recurse => true,
  source => "/vagrant/vagrant/modules/solr/"
}

file 
{ 
  "/opt/apache-solr-3.5.0/phpro/solr/phpro/conf/schema.xml":

  require => File["/opt/apache-solr-3.5.0/phpro/"],
  ensure => present,
  mode => 755,
  source => "/vagrant/vagrant/modules/solr/schema.xml-magento"
}

file 
{ 
  "/etc/init.d/solr-phpro":
  
  require => File["/opt/apache-solr-3.5.0/phpro/"],
  ensure => present,
  mode => 755,
  owner => root,
  group => root,
  source  => "/vagrant/vagrant/modules/solr/solr-phpro-magento"
}

service 
{ 
  "solr-phpro":
  
  require => File["/etc/init.d/solr-phpro"],
  ensure => running
}

#############
# Memcached #
#############

package
{
  "php5-memcache":

  ensure => "installed",
  require => Exec["apt-get update"]
}

}
