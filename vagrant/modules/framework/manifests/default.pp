#######################################################
# These packages will be installed in every framework #
#######################################################

class framework::default
{

notice('default.pp is loading')

#######################
# Update Package List #
#######################

exec
{
  "apt-get update":

  command => "/usr/bin/apt-get update",
  user    => "root",
  require => Exec["autoproxy"]
}

###############
# ImageMagick #
###############

package
{
  "imagemagick":

  ensure => "installed",
  require => Exec["apt-get update"]
}

########
# Htop #
########

package
{
  "htop":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#########
# Rsync #
#########

package
{
  "rsync":

  ensure => "installed",
  require => Exec["apt-get update"]
}

###########
# Postfix #
###########

package
{
  "postfix":

  ensure => "installed",
  require => Exec["apt-get update"]
}

file
{
  "main.cf":

  path    => "/etc/postfix/main.cf",
  ensure  => present,
  replace => true,
  owner   => root,
  group   => root,
  source  => "/vagrant/vagrant/modules/postfix/main.cf",
  require => Package["postfix"]
}

#######
# Git #
#######
 
package
{
  "git-core":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#######
# SVN #
#######

package
{  
  "subversion":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#############
# Memcached #
#############

package
{ 
  "memcached":

  ensure => "installed",
  require => Exec["apt-get update"]
}

service 
{
  "memcached":

  ensure => "running",
  require => Package["memcached"]
}

########################################################################
# 								       #
# Frameworks below are included if not in comment in Vagrantfile.local #
#								       #
########################################################################

###########
# Magento #
###########

if $hostname == 'Magento'
{
	include framework::magento
}

##########  
# Drupal #
##########

if $hostname == 'Drupal'
{
    include framework::drupal 
}

##################
# Zend Framework #
##################

if $hostname == 'zend'
{
    include framework::zend
}

###########
# MongoDB #
###########

if $hostname == 'mongodb'
{
    include framework::mongodb
}

# --> this is not a framework and should be moved to a module. 

###############################
# Zend Framework with PHP 5.4 #
###############################

if $hostname == 'zend2php54'
{
    include framework::zend2php54
}

# --> this should be changed to zend2 

}
