class framework::zend 
{

notice('zend.pp is loading')

include apache::apache
include php::php
include mysql::mysql
include phpmyadmin::phpmyadmin

#################
#link src folder#
#################

file 
{ 
  "/var/www/src":

  ensure => 'link',
  target => '/vagrant/src',
  owner => www-data,
  group => vagrant,
  mode => 755,
  require => Package["apache2"]
}
}
