class framework::drupal
{

notice('drupal.pp is loading')

include apache::apache
include php::php
include mysql::mysql
include phpmyadmin::phpmyadmin

#########
# Drush #
#########

package
{
  "drush":

  ensure => "installed",
  require => Exec["apt-get update"],
}

###########
# Compass #
###########

exec
{
  "install-compass":

  command => "/vagrant/vagrant/scripts/installcompass",
  require => Exec["apt-get update"],
  user => "root"
}

########
# Solr #
########

package
{
  "openjdk-6-jdk":

  ensure => "installed",
  require => Exec["apt-get update"]
}

########
# Solr #
########

exec
{
  "install-solr":

  command => "/vagrant/vagrant/scripts/installsolr-drupal",
  require => Package["openjdk-6-jdk"],
  user => "root"
}

file 
{
  "/opt/apache-solr-3.6.2/phpro/":

  require => Exec["install-solr"],
  ensure => directory,
  recurse => true,
  source => "/vagrant/vagrant/modules/solr/"
}

file
{
  "/opt/apache-solr-3.6.2/phpro/solr/phpro/conf/schema.xml":

  require => File["/opt/apache-solr-3.6.2/phpro/"],
  ensure => present,
  mode => 755,
  source => "/vagrant/vagrant/modules/solr/schema.xml-drupal"
}

file
{ 
  "/etc/init.d/solr-phpro":

  require => File["/opt/apache-solr-3.6.2/phpro/"],
  ensure => present,
  mode => 755,
  owner => root,
  group => root,
  source  => "/vagrant/vagrant/modules/solr/solr-phpro-drupal"
}

service 
{ 
  "solr-phpro":

  require => File["/etc/init.d/solr-phpro"],
  ensure => running
}

#########
# Unzip #
#########

package
{
  "unzip":

  ensure => "installed",
  require => Exec["apt-get update"],
  before => Exec["install-aegir"]
}

########
# Curl #
########

package
{
  "curl":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#########
# Aegir #
#########

exec
{
  "install-aegir":

  command => "/vagrant/vagrant/scripts/installaegir 2> /home/vagrant/aegirerrorlog",
  user => root,
  require => Exec["/vagrant/vagrant/scripts/mysql_secure_installation"]
}

file
{
  "/etc/apache2/conf.d/aegir.conf":

  require => Exec["install-aegir"],
  ensure => link,
  before => Service["apache2"],
  source => "/var/aegir/config/apache.conf"
}

exec
{
  "changeaegirpassword":

  command => "/vagrant/vagrant/scripts/changeaegirpassword",
  user => root,
  require => File["/etc/apache2/conf.d/aegir.conf"]
}

file 
{ 
  "/var/spool/cron/crontabs/aegir":

  require => Exec["install-aegir"],
  replace => true,
  mode => 600,
  owner => aegir,
  group => root,
  source => "/vagrant/vagrant/modules/aegircrontab"
}

exec
{
  "set-curlrc":

  command => "/vagrant/vagrant/scripts/setcurlrc",
  require => Exec["install-aegir"],
}

#########
# Samba #
#########

package
{
  "samba":

  ensure => present,
  require => Exec["install-aegir"]
}

file 
{ 
  "/etc/samba/smb.conf":

  require => Package["samba"],
  replace => true,
  source => "/vagrant/vagrant/modules/samba/smb.conf"
}

service
{
  "smbd":

  ensure => running,
  require => File["/etc/samba/smb.conf"]
}

#####################
# link sites folder #
#####################

file
{ 
  "/var/www/sites":
  
  ensure => 'link',
  target => '/vagrant/sites',
  owner => aegir,
  group => crontab,
  mode => 600,
  require => Package["apache2"]
}

}
