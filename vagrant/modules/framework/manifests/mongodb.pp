class framework::mongodb
{

notice('mongodb.pp is loading')
include apache::apache

###########
# PHP 5.4 #
###########

exec 
{ 
  "install-php5.4":
  
  command => "/vagrant/vagrant/scripts/installphp5.4",
  user => root,
  require => Package["python-software-properties"] 
}

package 
{
  "python-software-properties":

  ensure => present,
  require => Exec["apt-get update"]
}

package 
{  
  "php5":

  ensure => present,
  require => Exec["install-php5.4"]
}


###########
# MongoDB #
###########

exec 
{ 
  "install-mongodb":
  
  command => "/vagrant/vagrant/scripts/installmongodb 2> /home/vagrantmongo_err",
  user => root
}

package
{
  "mongodb-10gen":

  ensure => present, 
  require => Exec["install-mongodb"]
}


###################
# Link SRC Folder #
###################

file 
{ 
  "/var/www/src":

  ensure => 'link',
  target => '/vagrant/src',
  owner => www-data,
  group => vagrant,
  mode => 755,
  require => Package["apache2"]
}
}

