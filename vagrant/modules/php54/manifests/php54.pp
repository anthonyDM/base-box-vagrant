class php54::php54 
{

notice('php54.pp is loading')

#####################
# Load PHP 5.4 Repo #
#####################

exec
{
  "php54-repo":

  command => "/vagrant/vagrant/scripts/php54repo",
  require => Exec["apt-get update"],
  user => "root"
}

###########
# PHP 5.4 #
###########

package 
{
  "php5":

  ensure => "installed",
  require => [Exec["apt-get update"],Exec["php54-repo"]]
}

package 
{
  "php5-cli":
 
  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

file 
{ 
  "/etc/php5/apache2/php.ini":
  
  owner => root,
  group => root,
  mode => 644,
  source => "/vagrant/vagrant/modules/php/php.ini",
  require => Service["apache2"]
}

###########
# Mod APC #
###########

package 
{
  "php-apc":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

file 
{ 
  "/etc/php5/conf.d/apc.ini":

  owner => root,
  group => root,
  replace => true,
  mode => 644,
  source => "/vagrant/vagrant/modules/php/apc.ini",
  require => Package["php-apc"]
}

############
# Mod Curl #
############

package 
{
  "php5-curl":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

##########
# Mod GD #
##########

package 
{
  "php5-gd":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

##############
# Mod Mcrypt #
##############

package 
{
  "php5-mcrypt":
 
  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

#################
# Mod Memcached #
#################

package 
{
  "php5-memcached":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

#############
# Mod MySQL #
#############

package 
{
  "php5-mysql":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

############## 
# Mod XDebug #
##############

package 
{
  "php5-xdebug":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}

#####################
# Apache PHP Module #
#####################

package
{
  "libapache2-mod-php5":

  ensure => "installed",
  require => [Exec["apt-get update"], Exec["php54-repo"]]
}
}

