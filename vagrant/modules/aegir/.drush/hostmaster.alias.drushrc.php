<?php 
$aliases['hostmaster'] = array (
  'context_type' => 'site',
  'platform' => '@platform_hostmaster',
  'server' => '@server_master',
  'db_server' => '@server_master',
  'uri' => 'phprobox.aegir',
  'root' => '/var/aegir/hostmaster-6.x-1.9',
  'site_path' => '/var/aegir/hostmaster-6.x-1.9/sites/phprobox.aegir',
  'site_enabled' => true,
  'language' => 'en',
  'client_name' => 'admin',
  'aliases' => 
  array (
  ),
  'redirection' => false,
  'cron_key' => '',
  'profile' => 'hostmaster',
);
