<?php 
$aliases['platform_hostmaster'] = array (
  'context_type' => 'platform',
  'server' => '@server_master',
  'web_server' => '@server_master',
  'root' => '/var/aegir/hostmaster-6.x-1.9',
  'makefile' => '/var/aegir/.drush/provision/aegir.make',
);
