class mysql::mysql
{

notice('mysql.pp is loading')

#########
# MySQL #
#########

package
{
  "mysql-server":
  
  ensure => "installed",
  require => Exec["apt-get update"]
}

service 
{  
  "mysql":

  ensure => "running",
  require => Package["mysql-server"]
}

file
{
  "mysql-config":

  path    => "/etc/mysql/my.cnf",
  ensure  => present,
  replace => true,
  owner   => root,
  group   => root,
  mode    => "0640",
  source  => "/vagrant/vagrant/modules/mysql/my.cnf",
  require => Package["mysql-server"],
  notify  => Service["mysql"]
}

exec
{
  "/vagrant/vagrant/scripts/mysql_secure_installation":

  require => File["mysql-config"]
}

exec
{
  "set-mysql-root-password":

  command => "/vagrant/vagrant/scripts/setmysqlpass",
  require => Package["mysql-server"]
}

}
