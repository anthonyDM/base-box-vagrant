class php::php 
{

notice('php.pp is loading')

##########
# PHP5.3 #
##########

package 
{
  "libapache2-mod-php5":

  ensure => "installed",
  require => Exec["apt-get update"]
}

package 
{  
  "php5":

  ensure => "installed",
  require => Exec["apt-get update"]
}

package 
{
  "php5-cli":

  ensure => "installed",
  require => Exec["apt-get update"]
}

file 
{ 
  "/etc/php5/apache2/php.ini":

  owner => root,
  group => root,
  mode => 644,
  source => "/vagrant/vagrant/modules/php/php.ini",
  require => Service["apache2"]
}

###########
# Mod APC #
###########

package 
{
  "php-apc":

  ensure => "installed",
  require => Exec["apt-get update"]
}

file 
{ 
  "/etc/php5/conf.d/apc.ini":
  
  owner => root,
  group => root,
  replace => true,
  mode => 644,
  source => "/vagrant/vagrant/modules/php/apc.ini",
  require => Package["php-apc"]
}

############
# Mod Curl #
############

package 
{
  "php5-curl":

  ensure => "installed",
  require => Exec["apt-get update"]
}

##########
# Mod GD #
##########

package
{
  "php5-gd":
  
  ensure => "installed",
  require => Exec["apt-get update"]
}

##############
# Mod Mcrypt #
##############

package 
{
  "php5-mcrypt":
  
  ensure => "installed",
  require => Exec["apt-get update"]
}

#################
# Mod Memcached #
#################

package
{
  "php5-memcached":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#############
# Mod MySQL #
#############

package 
{
  "php5-mysql":

  ensure => "installed",
  require => Exec["apt-get update"]
}

#####################
# Apache PHP Module #
#####################

exec 
{ 
  "enable_mod_php5":

  command => "/usr/sbin/a2enmod php5",
  user    => "root",
  require => Package["apache2"],
  notify  => Service["apache2"]
}
}
