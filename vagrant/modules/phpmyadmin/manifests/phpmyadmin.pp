class phpmyadmin::phpmyadmin 
{

notice('phpmyadmin.pp is loading')

##############
# PHPMyAdmin #
##############

package 
{
  "phpmyadmin":

  ensure => "installed",
  require => Exec["apt-get update"]
}

file 
{ 
  "phpmyadmin-config":

  path    => "/etc/phpmyadmin/config-db.php",
  ensure  => present,
  replace => true,
  owner   => root,
  group   => root,
  mode    => "0644",
  source  => "/vagrant/vagrant/modules/phpmyadmin/config-db.php",
  require => Package["phpmyadmin"]
}

}
