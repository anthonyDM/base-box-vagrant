class apache::apache 
{

notice('apache.pp is loading')

###########�############
# Apache Installation #
#######################

package
{
  "apache2":ensure => "installed",

  require => Exec["apt-get update"]
}

service 
{
  "apache2":ensure => "running",
  
  require => Package["apache2"]
}

file 
{ 
  "apache-config":

  path    => "/etc/apache2/apache2.conf",
  ensure  => present,
  replace => true,
  owner   => root,
  group   => root,
  mode    => "0644",
  source  => "/vagrant/vagrant/modules/apache/apache2.conf",
  require => Package["apache2"]
}


###############
# Mod Headers #
###############

exec
{ 
  "enable_mod_headers":

  command => "/usr/sbin/a2enmod headers",
  user    => "root",
  require => Package["apache2"],
  notify  => Service["apache2"]
}

###############
# Mod Rewrite #
###############

exec
{ 
  "enable_mod_rewrite":

  command => "/usr/sbin/a2enmod rewrite",
  user    => "root",
  require => Package["apache2"],
  notify  => Service["apache2"]
}

#############
# Mod Proxy #
#############

exec
{ 

  "enable_mod_proxy":

  command => "/usr/sbin/a2enmod proxy",
  user    => "root",
  require => Package["apache2"],
  notify  => Service["apache2"]
}

##################
# Mod Proxy_http #
##################

exec 
{ 
  "enable_mod_proxy_http":

  command => "/usr/sbin/a2enmod proxy_http",
  user    => "root",
  require => Package["apache2"],
  notify  => Service["apache2"]
}

#############################
# Remove Default index.html #
#############################

file
{
  "/var/www/index.html":

  ensure => absent,
  require => Package["apache2"]
}

################
# Permissionss #
################

file
{
  "/var/www/":

  owner => www-data,
  group => vagrant,
  mode => 775,
  require => Package["apache2"]
}

#####################################
# Replace /var/www/dev.phpro.local/ #
#####################################

file
{
  "/var/www/dev.phpro.local/":

  owner => www-data,
  group => vagrant,
  mode => 775,
  require => Package["apache2"],
  ensure => directory,
  recurse => true,
  purge => true,
  replace => true,
  force => true,
  source => "/vagrant/vagrant/modules/dev.phpro.local/"
}

####################################
# Link /etc/apache/sites-available #
####################################

file
{
  "/etc/apache2/sites-available/dev":

  owner => root,
  group => root,
  mode => 644,
  require => Package["apache2"],
  ensure => 'link',
  recurse => true,
  target => "/vagrant/cfg/dev/vhosts/"
}

file
{
  "/etc/apache2/sites-available":

  owner => root,
  group => root,
  mode => 644,
  require => File["/etc/apache2/sites-available/dev"],
  ensure => 'link',
  recurse => true,
  target => "/vagrant/vagrant/cfg/vhosts/"
}

#####################
# Enable all vhosts #
#####################

file
{
  "/etc/apache2/sites-enabled/dev":

  owner => root,
  group => root,
  mode => 644,
  require => Package["apache2"],
  ensure => 'link',
  recurse => true,
  target => "/vagrant/cfg/dev/vhosts/"
}

file
{
  "/etc/apache2/sites-enabled":

  owner => root,
  group => root,
  mode => 644,
  require => File["/etc/apache2/sites-available/dev"],
  ensure => 'link',
  recurse => true,
  target => "/vagrant/vagrant/cfg/vhosts/"
}

}
