class pear::pear 
{

notice('pear.pp is loading')

package 
{
  "php-pear":
  
  ensure => "installed",
  require => Package["php5"]  
}

exec 
{ 
  "/usr/bin/pear upgrade":

  user => "root",
  require => Package["php-pear"]
}

exec 
{ 
  "/usr/bin/pear config-set auto_discover 1":

  require => Exec["/usr/bin/pear upgrade"]
}

file 
{ 
  "/tmp/pear/temp":
  
  require => Exec["/usr/bin/pear config-set auto_discover 1"],
  ensure => "directory",
  owner => "root",
  group => "root",
  mode => 777
}

exec 
{ 
  "/usr/bin/pear channel-discover pear.phpunit.de; true":
  
  require => File["/tmp/pear/temp"]
}

exec 
{ 
  "/usr/bin/pear clear-cache":
  
  require => Exec["/usr/bin/pear channel-discover pear.phpunit.de; true"],
  user => "root"
}

exec 
{ 
  "/usr/bin/pear upgrade phpunit/phpunit":

  require => Exec["/usr/bin/pear clear-cache"],
  user => "root"  
}
}
